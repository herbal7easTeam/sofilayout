import UIKit
import CoreData
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    static var navigationCoordinator: RootNavigationCoordinator!
    static var dependencyRegistry: DependencyRegistry!

    //TODO: jbott - 02.09.17 - move to Persistance Layer
    var shouldHideX = Variable(true) //I hate this - want to move to the Persistance Layer for in memory store
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

