import UIKit
import Swinject
import SwinjectStoryboard

protocol DependencyRegistry {
    var container: Container { get }
    var navigationCoordinator: RootNavigationCoordinator! { get }
    
    typealias rootNavigationCoordinatorMaker = (UIViewController) -> RootNavigationCoordinator
    func makeRootNavigationCoordinator(rootViewController: UIViewController) -> RootNavigationCoordinator
    
    func makePhotosController(for person: Person) -> PhotosPanelViewController
}

class DependencyRegistryImpl: DependencyRegistry {

    var container: Container
    var navigationCoordinator: RootNavigationCoordinator!
    

    init(container: Container) {
        
        Container.loggingFunction = nil
        
        self.container = container
        
        registerDependencies()
        registerPresenters()
        registerViewControllers()
    }
    
    func registerDependencies() {
        
        container.register(RootNavigationCoordinator.self) { (r, rootViewController: UIViewController) in
            return RootNavigationCoordinatorImpl(with: rootViewController, registry: self)
        }.inObjectScope(.container)
        
        container.register(NetworkInteractor.self    ) { _ in NetworkInteraction()     }.inObjectScope(.container)
        container.register(PersistanceInteractor.self) { _ in PersistanceInteraction() }.inObjectScope(.container)
        container.register(PersonTranslator.self     ) { _ in PersonTranslation()      }.inObjectScope(.container)
        
        container.register(TranslationLayer.self) { r in
            let personTranslator = r.resolve(PersonTranslator.self)!
            return TranslationLayerImpl(personTranslator: personTranslator)
        }.inObjectScope(.container)

        container.register(ModelInteractor.self){ r in
            ModelInteraction(networkInteractor: r.resolve(NetworkInteractor.self)!,
                         persistanceInteractor: r.resolve(PersistanceInteractor.self)!,
                              translationLayer: r.resolve(TranslationLayer.self)!)
        }.inObjectScope(.container)
    }
    
    func registerPresenters() {
        container.register(PhotosPanelPresenter.self) { (r, person: Person) in
            PhotosPanelPresentation(person: person, modelInteractor: r.resolve(ModelInteractor.self)!)
        }
        
        container.register(PeopleTablePresenter.self) { r in
            PeopleTablePresentation(modelInteractor: r.resolve(ModelInteractor.self)!)
        }
    }
    
    func registerViewControllers() {
        container.register(PhotosPanelViewController.self) { (r, person: Person) in
            let presenter = r.resolve(PhotosPanelPresenter.self, argument: person)!
            
            //NOTE: We don't have access to the constructor for this VC so we are using method injection
            
            let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "PhotosPanelViewController") as! PhotosPanelViewController
                vc.configure(with: presenter, navigationCoordinator: self.navigationCoordinator)
            
            return vc
        }
    }

    //MARK: - Maker Methods
    func makeRootNavigationCoordinator(rootViewController: UIViewController) -> RootNavigationCoordinator {
        navigationCoordinator = container.resolve(RootNavigationCoordinator.self, argument: rootViewController)!
        return navigationCoordinator
    }

    func makePhotosController(for person: Person) -> PhotosPanelViewController {
        return container.resolve(PhotosPanelViewController.self, argument: person)!
    }
}
