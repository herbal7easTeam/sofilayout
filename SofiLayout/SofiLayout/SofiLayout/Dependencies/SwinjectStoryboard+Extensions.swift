import UIKit
import Swinject
import SwinjectStoryboard

// This enables injection of the initial view controller from the app's main
//   storyboard project settings. So, this is the starting point of the
//   dependency tree.
extension SwinjectStoryboard {
    @objc class func setup() {
        if AppDelegate.dependencyRegistry == nil {
            AppDelegate.dependencyRegistry = DependencyRegistryImpl(container: defaultContainer)
        }
        
        let dependencyRegistry: DependencyRegistry = AppDelegate.dependencyRegistry
        
        func main() {
            dependencyRegistry.container.storyboardInitCompleted(PeopleTableViewController.self) { r, vc in
                
                let coordinator = dependencyRegistry.makeRootNavigationCoordinator(rootViewController: vc)
                
                setupData(resolver: r, navigationCoordinator: coordinator)
                
                let presenter = r.resolve(PeopleTablePresenter.self)!
                
                //NOTE: We don't have access to the constructor for this VC so we are using method injection
                vc.configure(with: presenter, navigationCoordinator: coordinator)
            }
        }

        func setupData(resolver r: Resolver, navigationCoordinator: RootNavigationCoordinator) {
            MockedWebServer.sharedInstance.start()
            setupDatabase(resolver: r)
            AppDelegate.navigationCoordinator = navigationCoordinator
        }

        func setupDatabase(resolver r: Resolver) {
            let modelInteractor = r.resolve(ModelInteractor.self)!
            modelInteractor.initDatabase()
            modelInteractor.loadData()
        }
        
        main()
    }
}
