import Foundation

enum Source: String {
    case local,
         network
}
