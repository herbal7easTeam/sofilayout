import Foundation
import Outlaw

struct Person: Deserializable {
    var id: String?
    var avatar: String?
    var bio: String?
    var firstName: String?
    var lastName: String?
    var title: String?
    
    init(id: String?, avatar: String?, bio: String?, firstName: String?, lastName: String?, title: String?) {
        self.id = id
        self.avatar = avatar
        self.bio = bio
        self.firstName = firstName
        self.lastName = lastName
        self.title = title
    }
    
    init(object: Extractable) throws {
        id = try? object.value(for: "id")
        avatar = try? object.value(for: "avatar")
        bio = try? object.value(for: "bio")
        firstName = try? object.value(for: "firstName")
        lastName = try? object.value(for: "lastName")
        title = try? object.value(for: "title")
    }
}
