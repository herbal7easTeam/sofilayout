import Foundation

typealias VoidClosure = () -> Void
typealias CoreDataPeopleClosure = ([CoreDataPerson]) -> Void
typealias PeopleClosure = ([Person])->Void
typealias PeopleAndSourceBlock = (Source, [Person])->Void
typealias BlockWithSource = (Source)->Void
