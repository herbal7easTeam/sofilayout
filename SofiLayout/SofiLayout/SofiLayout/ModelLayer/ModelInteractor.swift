import Foundation
import RxSwift
import RxCocoa

protocol ModelInteractor {
    var source: Variable<Source?> { get }
    var people: Variable<[Person]> { get }
    
    func initDatabase()
//    func loadAllPeople()
    func loadData()
}

class ModelInteraction: ModelInteractor {
    
    let source = Variable<Source?>(nil)
    let people = Variable<[Person]>([])
    
    private let bag = DisposeBag()
    private let networkInteractor: NetworkInteractor
    private let persistanceInteractor: PersistanceInteractor
    private let translationLayer: TranslationLayer
    
    init(networkInteractor: NetworkInteractor,
     persistanceInteractor: PersistanceInteractor,
          translationLayer: TranslationLayer)
    {
        self.networkInteractor = networkInteractor
        self.persistanceInteractor = persistanceInteractor
        self.translationLayer = translationLayer
    }
    
    func initDatabase() {
        persistanceInteractor.initDatabase()
    }
    
//    func loadAllPeople() { //result may immediate, but use async callbacks
//        persistanceInteractor.loadAllPeople { coreDataPeople in //not using [weak self] - assuming that we have bigger problems if the model layer doesn't exist
//            let people = coreDataPeople.map(self.translationLayer.convert)
//            self.people.value = people
//        }
//    }
    
    func loadFromServer() {
        networkInteractor.loadFromServer { data in
            let people = self.translationLayer.people(from: data)
            print("🕵️‍♀️ network results:")
            people.forEach { print($0.fullName) }
            
            self.persistanceInteractor.save(people, translationLayer: self.translationLayer) {
                self.loadFromDB(from: .network)
            }
        }
    }
    
    func loadData() {
        func mainWork() {
            loadFromDB(from: .local)
            
            persistanceInteractor.databaseReady.asObservable().subscribe(onNext: { isReady in
                guard isReady else { return }
                
                self.loadFromServer()
            }).disposed(by: bag)
        }
        mainWork()
    }
    
    func loadFromDB(from source: Source) {
        persistanceInteractor.loadFromDB { coreDataPeople in
            let people = self.translationLayer.convert(from: coreDataPeople)
            print("🦄 database results. source:\(source)")
            people.forEach { print($0.fullName) }

            self.people.value = people
            self.source.value = source
        }
    }

}
