import Foundation
import Alamofire

protocol NetworkInteractor {
    func loadFromServer(finished: @escaping (Data) -> Void)
}

class NetworkInteraction: NetworkInteractor {
    func loadFromServer(finished: @escaping (Data) -> Void) {
        Alamofire.request("http://localhost:8080/people")
            .responseJSON
            { response in
                guard let data = response.data else { return }
                
                finished(data)
        }
    }
}
