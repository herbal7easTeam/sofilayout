import Foundation
import CoreData
import RxSwift
import Outlaw

protocol PersistanceInteractor {
    
    var databaseReady: Variable<Bool> { get }
    
    func initDatabase()
    func loadAllPeople(finished: @escaping CoreDataPeopleClosure)
    func save(_ people: [Person], translationLayer: TranslationLayer, finished: @escaping () -> Void)
    func loadFromDB(finished: @escaping CoreDataPeopleClosure)
}

class PersistanceInteraction: PersistanceInteractor {
    
    private var bag = DisposeBag()
    var databaseReady = Variable(false)
    
    
    private var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "SofiLayout")

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}

//MARK: Public API
extension PersistanceInteraction {

    func loadFromDB(finished: @escaping CoreDataPeopleClosure) {
        databaseReady.asObservable().subscribe(onNext: { isReady in
            guard isReady else { return }
            
            print("loading data locally")
            let coreDataPeople = self.loadSpiesFromDB()
            finished(coreDataPeople)
        }).disposed(by: bag)
    }

    
    func save(_ people: [Person], translationLayer: TranslationLayer, finished: @escaping () -> Void) {
        databaseReady.asObservable().subscribe(onNext: { isReady in
            guard isReady else { return }
            
            self.clearOldResults()

            _ = translationLayer.convert(from: people, with: self.mainContext)
            
            try! self.mainContext.save()
            
            finished()
        }).disposed(by: bag)
    }
    
    func loadAllPeople(finished: @escaping CoreDataPeopleClosure) { //async api
        
        
        databaseReady.asObservable().subscribe(onNext: { isReady in
            guard isReady else { return }
            
            //assume a large data set in backend
            DispatchQueue.global().async { [weak self] in
                guard let strongSelf = self else { return }
                
                let sort = NSSortDescriptor(key:"id", ascending: true)
                
                let fetchRequest: NSFetchRequest<CoreDataPerson> = CoreDataPerson.fetchRequest()
                    fetchRequest.sortDescriptors = [sort]
                
                let coreDataPeople = try! strongSelf.persistentContainer.viewContext.fetch(fetchRequest)
                
                finished(coreDataPeople)
            }
        }).disposed(by: bag)
    }
}

//MARK: Database Setup Methods
extension PersistanceInteraction {

    fileprivate func loadSpiesFromDB() -> [CoreDataPerson] {
        let sortOn = NSSortDescriptor(key: "id", ascending: true)
        
        let fetchRequest: NSFetchRequest<CoreDataPerson> = CoreDataPerson.fetchRequest()
        fetchRequest.sortDescriptors = [sortOn]
        
        let people = try! persistentContainer.viewContext.fetch(fetchRequest)
        
        return people
    }
    
    func initDatabase() {
        DispatchQueue.global().async { //not using weak, assuming we have bigger problems if our main singleton datalayer doesn't exist
            self.clearOldResults()
            self.preloadDatabase {
                DispatchQueue.main.async {
                    self.databaseReady.value = true
                }
            }
        }
    }

    func preloadDatabase(finish:@escaping () -> Void) {
        
        let people = loadDBJSON()
        
        //act like we are parsing and creating new objects
        self.persistentContainer.performBackgroundTask{ context in
            var coreDataPeople = [CoreDataPerson]()
            
            //TODO: jbott - 02.10.18 - move to injected translator instead
            //hacked for now, this is just to load sample data, and does not fall into the normal application flow or use injection properly
            let translator: PersonTranslator = PersonTranslation()

            let initialPeopleLimit = 2
            
            for i in 0 ..< initialPeopleLimit {
                let person = people[i]
                let coreDataPerson = translator.convert(person: person, context: context)
                coreDataPeople.append(coreDataPerson)  //retain it through the for loop
            }
            
            try! context.save()
            
            DispatchQueue.main.async {
                finish()
            }
        }
    }
    

    
    
    func clearOldResults() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "CoreDataPerson") // = CoreDataPerson.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        //I want this to crash if I can't get a clean slate for the examples
        try! persistentContainer.persistentStoreCoordinator.execute(deleteRequest, with: persistentContainer.viewContext)
        
        persistentContainer.viewContext.reset()
    }

}

//MARK: Helper Methods
extension PersistanceInteraction {
    private func loadDBJSON() -> [Person] {
        let url = Bundle.main.url(forResource: "team", withExtension: "json")!
        let people: [Person] = try! JSON.value(from: url)
        
        return people
    }
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
