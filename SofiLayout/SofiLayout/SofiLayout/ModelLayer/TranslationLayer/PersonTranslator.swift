import Foundation
import CoreData
import Outlaw

protocol PersonTranslator {
    func convert(person: Person, context: NSManagedObjectContext) -> CoreDataPerson
    func convert(coreDataPerson: CoreDataPerson) -> Person
}

class PersonTranslation: PersonTranslator {
    func convert(person: Person, context: NSManagedObjectContext) -> CoreDataPerson {
        
        let coreDataPerson = CoreDataPerson(context: context)
            coreDataPerson.id        = person.id
            coreDataPerson.avatar    = person.avatar
            coreDataPerson.bio       = person.bio
            coreDataPerson.firstName = person.firstName
            coreDataPerson.lastName  = person.lastName
            coreDataPerson.title     = person.title
        
        return coreDataPerson
    }
    
    func convert(coreDataPerson: CoreDataPerson) -> Person {
        let person = Person(id: coreDataPerson.id,
                        avatar: coreDataPerson.avatar,
                           bio: coreDataPerson.bio,
                     firstName: coreDataPerson.firstName,
                      lastName: coreDataPerson.lastName,
                         title: coreDataPerson.title)
        return person
    }
    
}
