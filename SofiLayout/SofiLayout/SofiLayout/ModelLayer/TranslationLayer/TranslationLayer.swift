import Foundation
import CoreData
import Outlaw

protocol TranslationLayer {
    func convert(person: Person, context: NSManagedObjectContext) -> CoreDataPerson
    func convert(from people: [Person], with context: NSManagedObjectContext) -> [CoreDataPerson]
    
    func convert(coreDataPerson: CoreDataPerson) -> Person
    func convert(from coreDataPeople:[CoreDataPerson]) -> [Person]
    
    func people(from data: Data) -> [Person]
}

class TranslationLayerImpl: TranslationLayer {
    
    private var personTranslator: PersonTranslator
    
    init(personTranslator: PersonTranslator) {
        self.personTranslator = personTranslator
    }
    
    func convert(person: Person, context: NSManagedObjectContext) -> CoreDataPerson {
        return personTranslator.convert(person: person, context: context)
    }
    
    func convert(coreDataPerson: CoreDataPerson) -> Person {
        return personTranslator.convert(coreDataPerson: coreDataPerson)
    }
    
    func convert(from people: [Person], with context: NSManagedObjectContext) -> [CoreDataPerson] {
        print("convering DTOs to Core Data Objects")
        let coreDataPeople = people.flatMap{ convert(person: $0, context: context) } // keeping it simple by keeping things single threaded
        
        return coreDataPeople
    }
    
    func convert(from coreDataPeople:[CoreDataPerson]) -> [Person] {
        let people = coreDataPeople.flatMap { convert(coreDataPerson: $0) }
        
        return people
    }
    
    func people(from data: Data) -> [Person] {
        let people:[Person] = try! JSON.value(from: data)
        
        return people
    }
}


