import UIKit

protocol RootNavigationCoordinator {
    func rowSelected(for person: Person)
}

class RootNavigationCoordinatorImpl: RootNavigationCoordinator {
    
    var registry: DependencyRegistry
    var rootViewController: UIViewController

    init(with rootViewController: UIViewController, registry: DependencyRegistry) {
        self.rootViewController = rootViewController
        self.registry = registry
    }
    
    func rowSelected(for person: Person) {
        let vc = registry.makePhotosController(for: person)
        
        rootViewController.navigationController?.pushViewController(vc, animated: true)
    }
}
