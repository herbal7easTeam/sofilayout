import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

//Random element from array
//https://stackoverflow.com/questions/24003191/pick-a-random-element-from-an-array
extension Array {
    func randomItems(numberOfItems: Int) -> [Element?] {
        var result = [Element?]()
        
        for _ in 0 ..< numberOfItems {
            let item = randomItem()
            result.append(item)
        }
        
        return result
    }
    
    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}


//MARK: - Navbar helpers
//Code to hide hairline between navbar and toolbar
//https://stackoverflow.com/questions/34452920/removing-the-hairline-under-navigation-bar?noredirect=1&lq=1
protocol HideableHairlineViewController {
    
    func hideHairline()
    func showHairline()
}

extension HideableHairlineViewController where Self: UIViewController {
    
    func hideHairline() {
        findHairline()?.isHidden = true
    }
    
    func showHairline() {
        findHairline()?.isHidden = false
    }
    
    private func findHairline() -> UIImageView? {
        return navigationController?.navigationBar.subviews
            .flatMap { $0.subviews }
            .flatMap { $0 as? UIImageView }
            .filter { $0.bounds.size.width == self.navigationController?.navigationBar.bounds.size.width }
            .filter { $0.bounds.size.height <= 2 }
            .first
    }
    
}

//MARK - Entity Extensions
extension Person {
    var fullName: String {
        let firstName = self.firstName ?? ""
        let lastName  = self.lastName  ?? ""
        
        return "\(firstName) \(lastName)" //english centric, not very locale friendly
    }
}
