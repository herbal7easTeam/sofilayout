import UIKit
import RxSwift
import RxCocoa
import AlamofireImage

class ImageView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var xButton: UIButton!
    
    let hideImageView = Variable(false)
    let bag = DisposeBag()
    
    //TODO: jbott - 02.09.17 - move to Persistance Layer
    var shouldHideX: Variable<Bool> {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.shouldHideX
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    
    func commonInit() {
        loadView()
        setupEvents()
    }
    
    func loadView() {
        Bundle.main.loadNibNamed("ImageView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    func setupEvents() {
        
        shouldHideX.asDriver().drive(xButton.rx.isHidden).disposed(by: bag)
        hideImageView.asDriver().drive(imageView.rx.isHidden).disposed(by: bag)
        
        let longPressRecoginizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
        addGestureRecognizer(longPressRecoginizer)
        
        let tapRecoginizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        addGestureRecognizer(tapRecoginizer)
    }

    @objc func longPress() {
        print("long Pressed")
        shouldHideX.value = false
    }
    
    @objc func tapped() {
        print("tapped")
        shouldHideX.value = true
    }
    
    @IBAction func xTapped() {
        print("xtapped")
        hideImageView.value = true
        shouldHideX.value   = true
    }
    
    func loadImage(from url: URL) {
        let placeholderImage = #imageLiteral(resourceName: "SampleImage")
        imageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
    }

}
