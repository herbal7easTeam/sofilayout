import Foundation
import RxSwift

protocol PeopleTablePresenter {
    var people: Variable<[Person]> { get }
}

class PeopleTablePresentation: PeopleTablePresenter {
    var people: Variable<[Person]>   { return modelInteractor.people } //bubbling up the Variable from lower layers
    
    private var modelInteractor: ModelInteractor
    
    init(modelInteractor: ModelInteractor) {
        self.modelInteractor = modelInteractor
    }
}
