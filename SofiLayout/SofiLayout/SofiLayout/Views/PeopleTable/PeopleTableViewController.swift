import UIKit
import RxSwift
import RxCocoa

class PeopleTableViewController: UITableViewController {

    private var defaultCellId = "defaultCellId"

    private var presenter: PeopleTablePresenter!
    private var navigationCoordinator: RootNavigationCoordinator!
    private var bag = DisposeBag()

    func configure(with presenter: PeopleTablePresenter,
                   navigationCoordinator: RootNavigationCoordinator)
    {
        self.presenter = presenter
        self.navigationCoordinator = navigationCoordinator
        
        setupData()
    }
    
    func setupData() {
        presenter.people.asObservable().subscribe(onNext: { [weak self] people in
            self?.tableView.reloadData()
        }).disposed(by: bag)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: defaultCellId)
    }
}

// MARK: - TableViewDataSource
extension PeopleTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.people.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let person = presenter.people.value[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: defaultCellId, for: indexPath)
            cell.configure(with: person)
        
        return cell
    }
}

//MARK: - TableDelegate Methods
extension PeopleTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let person = presenter.people.value[indexPath.row]
        navigationCoordinator.rowSelected(for: person)
    }
}

//private extension methods - normally put this in a custom cell class
private extension UITableViewCell {
    func configure(with person: Person) {
        self.textLabel?.text = person.fullName
    }
}
