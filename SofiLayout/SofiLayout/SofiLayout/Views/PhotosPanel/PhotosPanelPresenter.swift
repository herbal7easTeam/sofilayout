import Foundation
import RxSwift
import RxCocoa

protocol PhotosPanelPresenter {
    var source: Variable<Source?> { get }
    var people: Variable<[Person]> { get }
    var person: Person { get }
}

class PhotosPanelPresentation: PhotosPanelPresenter {

    var source: Variable<Source?> { return modelInteractor.source }
    var people: Variable<[Person]>   { return modelInteractor.people } //bubbling up the Variable from lower layers
    var person: Person
    
    private var modelInteractor: ModelInteractor

    init(person: Person, modelInteractor: ModelInteractor) {
        self.person = person
        self.modelInteractor = modelInteractor
    }
}
