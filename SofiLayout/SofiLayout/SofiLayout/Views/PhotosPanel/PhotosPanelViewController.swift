import UIKit
import RxSwift
import Alamofire
import AlamofireImage
import MessageUI

class PhotosPanelViewController: UIViewController, UINavigationBarDelegate, HideableHairlineViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var bioLabel: UILabel!
    @IBOutlet var mainImageView: ImageView!
    @IBOutlet var sideImage1: ImageView!
    @IBOutlet var sideImage2: ImageView!
    @IBOutlet var bottomImage1: ImageView!
    @IBOutlet var bottomImage2: ImageView!
    @IBOutlet var bottomImage3: ImageView!
    
    private var images = Variable<[UIImage]>([])
    private var imageViews: [ImageView]!
    private var bag = DisposeBag()
    
    private var presenter: PhotosPanelPresenter!
    private var navigationCoordinator: RootNavigationCoordinator!
    
    override func viewDidLoad() {
        setup(with: presenter.person)
        imageViews = [sideImage1, sideImage2, bottomImage1, bottomImage2, bottomImage3]
        hideHairline()
        setupData()
    }

    func setup(with person: Person) {
        bioLabel.text = person.bio
        
        if let urlString = person.avatar, let url = URL(string: urlString) {
            mainImageView.loadImage(from: url)
        }
    }
    
    func configure(with presenter: PhotosPanelPresenter,
            navigationCoordinator: RootNavigationCoordinator)
    {
        self.presenter = presenter
        self.navigationCoordinator = navigationCoordinator
    }
    
    func setupData() {
        
        presenter.source.asObservable().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] source in
            guard let strongSelf = self, let _ = source else { return }
            
            let people = strongSelf.presenter.people.value
            let urlStrings = people.flatMap { $0.avatar }
            let urls = urlStrings.flatMap { URL(string: $0) }
            
            strongSelf.loadImages(for: urls)
        }).disposed(by: bag)
    }
    
    func loadImages(for urls: [URL]) {
        let fiveUrls = urls.randomItems(numberOfItems: 5).flatMap { $0 }
        for (index, url) in fiveUrls.enumerated() {
            imageViews[index].loadImage(from: url)
        }
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        print("save tapped")
        let screenshot = renderScreenShot()
        email(screenshot)
    }
    
    func email(_ screenshot: UIImage) {
        guard let imageData: Data = UIImagePNGRepresentation(screenshot) else { return }
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["bsoumpholphakdy@sofi.org", "mjaffa@sofi.org", "tlawson@sofi.org"]) //NOTE: though not in the requriements, I changed the email from: mjaffa.sofi.org to mjaffa@sofi.org
                mail.setSubject("Screen shot for layout test")
                mail.setMessageBody("Thank you for the opportunity to review all these concepts.", isHTML: false)
                mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "screenshot")
            
            present(mail, animated: true)
        }
    }
    
    func renderScreenShot() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: view.frame.size.width, height: view.frame.size.height))
        _ = UIGraphicsGetCurrentContext()
        view.drawHierarchy(in: view.frame, afterScreenUpdates: true)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return screenShot!
    }
}

//MARK: - MFMailComposeViewControllerDelegate
extension PhotosPanelViewController {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

//MARK: - UINavigationBarDelegate
extension PhotosPanelViewController {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

